package com.jet.practice.WayneAirlinesRegistry;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class WayneAirlinesRegistryApplication {

	public static void main(String[] args) {
		SpringApplication.run(WayneAirlinesRegistryApplication.class, args);
	}
}
